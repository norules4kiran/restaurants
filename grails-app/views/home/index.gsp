<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Restaurants</title>
    <asset:stylesheet src="home.css"/>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

</head>

<body>
<div class="container restaurants" style="margin-top: 20px" data-restaurants="${restaurants as grails.converters.JSON}">
    <div class="row">
        <form id="searchForm">
            <div class="input-group">
                <input type="text" name="location" class="form-control" value="${location}">
                <input type="submit" style="display: none;">
                <span class="input-group-addon submit" style="cursor: pointer">Search</span>
            </div>
        </form>
        <hr>
    </div>
    <div class="row" id="map-canvas">

    </div>
</div>
<asset:javascript src="home.js"/>
<script type="text/javascript">
    var restaurants = $(".restaurants").attr("data-restaurants");
    restaurants = JSON.parse(restaurants);


    function initialize() {
        var mapOptions = {
            zoom: 10,
            center: new google.maps.LatLng(restaurants[0][1], restaurants[0][2])
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

        setMarkers(map, restaurants);
    }


    function setMarkers(map, locations) {

        var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18 , 1],
            type: 'poly'
        };
        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                //icon: {url:'../images/beachflag.png',size: new google.maps.Size(20, 32),origin: new google.maps.Point(0,0),anchor: new google.maps.Point(0, 32)},
                shape: shape,
                title: beach[0],
                zIndex: beach[3]
            });
            /*var infowindow = new google.maps.InfoWindow({
                content: beach[0]+"\n"+beach[4]
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(marker.get('map'), marker);
            });*/
            attachSecretMessage(marker, i);
        }
    }

    function attachSecretMessage(marker, num) {
        var message = ['This', 'is', 'the', 'secret', 'message'];
        var infowindow = new google.maps.InfoWindow({
            content: "<b>"+restaurants[num][0]+"</b><br>"+restaurants[num][4]
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(marker.get('map'), marker);
        });
    }


    google.maps.event.addDomListener(window, 'load', initialize);
</script>
</body>
</html>