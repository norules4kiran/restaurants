package restaurants

import grails.converters.JSON

class HomeController {

    def index() {
        def key = "AIzaSyCLJtYC6T0FkxUH0FE-jAVqSCmPrr9XI4c"
        def restaurants = []

        def location = params.location
        if(location){
            def city = java.net.URLEncoder.encode(location,"UTF-8")
            def url = new URL("https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants+in+"+city+"&key="+key)
            print(url.newReader())
            def response = JSON.parse(url.newReader()) // response is an instance of JSONObject (see Grails API docs)
            def results = []
            results= response.results
            results.eachWithIndex{ result, int i ->
                def restaurant = []
                restaurant.add(result.name)
                restaurant.add(result.geometry.location.lat)
                restaurant.add(result.geometry.location.lng)
                restaurant.add(i)
                restaurant.add(result.formatted_address)

                restaurants.add(restaurant)
            }
        }

        [restaurants:restaurants,location:location]
    }
}
